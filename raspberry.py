

import cv2
import numpy as np
import requests
from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import json as json

#print('1')

content_type = 'image/jpeg'
headers = {'content-type': content_type}


camera = PiCamera()
camera.resolution = (1280, 720)
time.sleep(0.5)


#print('2')

# image = camera.capture(
	# 'image.jpg',
	# rawCapture,
	# format="jpeg")

# image = rawCapture.array

time.sleep(0.1)

server_address = "http://192.168.0.102:8000/server"

def send_image(image):
	#print('1.1.1')
	_, img_encoded = cv2.imencode('.jpg', image)
	#print('1.1.2')
	response = requests.post(server_address, data=img_encoded.tostring(), headers=headers)
	#print('1.1.3')
	#print (json.loads(response.text))


#print('3')

while True:

	#print('1.1')

	camera.capture('image.jpg', format="jpeg")

	#print('1.2')

	image = cv2.imread('image.jpg')

	send_image(image)

	#print('1.3')

	time.sleep(0.25)



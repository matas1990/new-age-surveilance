
from flask import Flask, request, Response, send_file
import jsonpickle
from bs4 import BeautifulSoup, NavigableString
import re
import datetime
#import urllib3

import cv2
from keras.models import load_model
import numpy as np

from utils.datasets import get_labels
from utils.inference import detect_faces
from utils.inference import draw_text
from utils.inference import draw_bounding_box
from utils.inference import apply_offsets
from utils.inference import load_detection_model
from utils.inference import load_image
from utils.preprocessor import preprocess_input

# Initialize the Flask application
app = Flask(__name__)

# Paths for models
detection_model_path = 'models/haarcascade_frontalface_default.xml'
emotion_model_path = 'models/fer2013_mini_XCEPTION.102-0.66.hdf5'
gender_model_path = 'models/simple_CNN.81-0.96.hdf5'


age_model = "https://github.com/yu4u/age-gender-estimation/releases/download/v0.5/weights.18-4.06.hdf5"
modhash = '89f56a39a78454e96379348bddd78c0d'

# Load labels
emotion_labels = get_labels('fer2013')
gender_labels = get_labels('imdb')

# Load models
face_detection = load_detection_model(detection_model_path)
emotion_classifier = load_model(emotion_model_path, compile=False)
gender_classifier = load_model(gender_model_path, compile=False)

# Offsets
gender_offsets = (30, 60)
emotion_offsets = (20, 40)

# getting input model shapes for inference
emotion_target_size = emotion_classifier.input_shape[1:3]
gender_target_size = gender_classifier.input_shape[1:3]

# Keeping insights in chunks
insight_chunks = []

id_html = 1

# Takes a photo and feeds server information about it
def extract_insights(photo):
    print('1')
    gray_image = cv2.cvtColor(photo, cv2.COLOR_BGR2GRAY)
    rgb_image = cv2.cvtColor(photo, cv2.COLOR_BGR2RGB)
    faces = detect_faces(face_detection, gray_image)

    print(faces)

    #ages = predict_age(faces)

    for face_coordinates in faces:
        print('3')
        x1, x2, y1, y2 = apply_offsets(face_coordinates, gender_offsets)
        rgb_face = rgb_image[y1:y2, x1:x2]
        print('4')
        x1, x2, y1, y2 = apply_offsets(face_coordinates, emotion_offsets)
        gray_face = gray_image[y1:y2, x1:x2]
        try:
            rgb_face = cv2.resize(rgb_face, (gender_target_size))
            gray_face = cv2.resize(gray_face, (emotion_target_size))
        except:
            continue
        print('5')

        gray_face = preprocess_input(gray_face, False)
        gray_face = np.expand_dims(gray_face, 0)
        gray_face = np.expand_dims(gray_face, -1)
        emotion_label_arg = np.argmax(emotion_classifier.predict(gray_face))
        emotion_text = emotion_labels[emotion_label_arg]
        print('6')
        rgb_face = np.expand_dims(rgb_face, 0)
        rgb_face = preprocess_input(rgb_face, False)
        gender_prediction = gender_classifier.predict(rgb_face)
        gender_label_arg = np.argmax(gender_prediction)
        gender_text = gender_labels[gender_label_arg]

        print('7')
        print(emotion_text)
        print(gender_text)
        append_data(gender_text, emotion_text)


# Remove same person in 1 minute intervals and such
def modify_chunk():
    for i in insights:
        pass

# Appends a new row to html
def append_data(gender, emotion):
    soup = BeautifulSoup(open("index.html"), "html.parser")
    information_table = soup.find('tbody')
    information_table.insert(0, BeautifulSoup('<tr><td>{}.</td><td>{}</td><td>{}</td><td>{}:{}:{}</td></tr>'.format(id_html, gender, emotion, datetime.datetime.now().hour, datetime.datetime.now().minute, datetime.datetime.now().second), 'html.parser'))
    globals()['id_html'] += 1    

    with open("index.html", "w") as outf:
        outf.write(str(soup))

@app.route('/')
def main_file():
    return send_file('index.html')
@app.route('/style/skeleton.css')
def ccs1():
    return send_file('style/skeleton.css')
@app.route('/style/normalize.css')
def css2():
    return send_file('style/normalize.css')
@app.route('/delete_data/')
def delete_data():
    id_html = 1
    soup = BeautifulSoup(open("index.html"), "html.parser")
    soup.find('tbody').decompose()
    soup.find('table').append(BeautifulSoup('<tbody></tbody>'))
    with open("index.html", "w") as outf:
        outf.write(str(soup))
    return ""

    # route http posts to this method
@app.route('/server', methods=['POST'])

def server():
    r = request
    # convert string of image data to uint8
    nparr = np.fromstring(r.data, np.uint8)
    # decode image
    img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
    cv2.imwrite('test.jpg', img)


    extract_insights(img)

    # do some fancy processing here....

    # build a response dict to send back to client
    response = {'message': 'image received. size={}x{}'.format(img.shape[1], img.shape[0])
                }
    # encode response using jsonpickle
    response_pickled = jsonpickle.encode(response)

    return Response(response=response_pickled, status=200, mimetype="application/json")

# start flask app
app.run(host="0.0.0.0", port=8000)